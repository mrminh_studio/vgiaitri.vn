var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var _ = require('underscore');

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket){
  socket.on('login', function(data, pass){
	console.log('a user login: ', data.username);
	var user = doLogin(data.username, data.pass);
	console.log('logeed as', user);
	
    socket.emit('logged in', user);
	socket.emit('sync fish', {fishes: getFishes()});
  });
  
  socket.on('shoot fish', function(data, pass){
	
	console.log('a user shoot: ', data.token, data.fishes, data.position);
	  //if(users[data.username] === undefined) return;
	result = processShoot(data.token, data.level, data.fishes, data.position);
	socket.broadcast.emit('user shoot', result);
	socket.emit('user shoot', result);
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

fishes = {};

fishInfos = {
	0: {
		ratio: 90,
		hp: 2,
		quantity: 30,
		price: 2
	},
	1: {
		ratio: 20,
		hp: 10,
		quantity: 5,
		price: 7
	},
	2: {
		ratio: 10,
		hp: 50,
		quantity: 2,
		price: 30
	}
}

fillWithFishes();

users = {};
function doLogin(username, pass){
	if(users[username] === undefined) {
		users[username] = {
			"username": username,
			"money": 10000,
			"token": username
		}
	}
	return users[username];
}

function processShoot(username, level, fs, pos){
	if(users[username] === undefined) return;
	users[username].money = parseInt(users[username].money, 10) - level + 1;
	dieFishes = [];
	for(var i in fs){
		if(fishes[fs[i]] === undefined) continue;
		if(fishes[fs[i]].Die) continue;
		ff = fishes[fs[i]];
		fishes[fs[i]].HP -= level + 1;
		if((Math.random() * ff.HP) < 1.5){
			dieFishes.push(fishes[fs[i]].ID);
			fishes[fs[i]].Die = true;
			console.log(ff.ID, ff.HP, " die");
			fishes = _.omit(fishes, [fishes[fs[i]].ID]);
			users[username].money = parseInt(users[username].money, 10) + parseInt(ff.Price);
		} 
	}
	fillWithFishes();
	return {
		fishes: dieFishes,
		money: users[username].money,
		position: pos,
		username: username
	}
}

function getFishes(){
	return _.values(fishes);
}

function addRandomFish(){
	typeId = Math.floor(Math.random() * 3);
	while(fishInfos[typeId].ratio < (Math.random() * 100)){
		typeId = Math.floor(Math.random() * 3);
	}
	addFish(typeId);
}

function fillWithFishes(){
	c = _.values(fishes).length;
	for(var i=c; i<25; i++){
		addRandomFish();
	}
}

function addFish(fishInfoId){
	info = fishInfos[fishInfoId];
	fish = {
		ID: Math.floor(Math.random() * 1000000),
		TypeId: fishInfoId,
		MovementType: "Circle",
		Velocity: 5 + Math.floor(Math.random() * 5),
		Created: (new Date()).toISOString(),
		MovementStartAt: Math.floor(Math.random() * 360),
		CircleRadius: 5,
		CircleCenterPoint: {
			x: Math.floor(Math.random() * 20) - 10, 
			z: Math.floor(Math.random() * 20) - 10, 
			y: 0
		},
		
		HP: info.hp,
		Price: info.price,
		Die: false
	}
	fishes[fish.ID] = fish;
}