﻿using UnityEngine;
using System.Collections;

public class GunType {
	public Sprite NormalState;
	public Sprite ShotState;
}

public class Gun : MonoBehaviour {
	[SerializeField]
	public Sprite[] NormalSprites;
	public Sprite[] ShotSprites;
	public int Level = 0;
	private SpriteRenderer spriteRenderer;
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();
		if (NormalSprites.Length > 0) {
			spriteRenderer.sprite = NormalSprites[0];
		}

	}

	void ChangeLevel(int level) {
		Level = level;
		ResetAnim ();
	}

	void NextLevel(){
		Level++;
		if (Level > NormalSprites.Length - 1)
			Level = 0;
		ResetAnim ();
	}

	void PreviousLevel(){
		Level--;
		if (Level < 0)
			Level = NormalSprites.Length - 1;
		ResetAnim ();
	}

	void ResetAnim () {
		spriteRenderer.sprite = NormalSprites [Level];
	}

	void ShootAnim () {
		spriteRenderer.sprite = ShotSprites [Level];
		Invoke ("ResetAnim", 0.25f);
	}

	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			ShootAnim();
		}
		else
		if (Input.GetMouseButtonDown (1)) {
			NextLevel();
		}
	}
}
