﻿using UnityEngine;
using System.Collections;
using System;

public class MoveCircle : Move {

	public DateTime StartTime = DateTime.Now;
	public float Velocity = 10f;
	public float StartDegree = 0;
	public float Radius = 4f;
	public Vector3 CenterPoint;

	float currentDegree;

	void Update () {
		currentDegree = (float)(DateTime.Now - StartTime).TotalSeconds * Velocity + StartDegree;
		float rad = currentDegree * Mathf.Deg2Rad;
		float dx = Mathf.Cos (rad);
		float dz = Mathf.Sin (rad);
		transform.position = CenterPoint + new Vector3 (dx, 0, dz) * Radius;
	}

	void FixedUpdate(){
		transform.rotation = Quaternion.AngleAxis (-currentDegree, Vector3.up);
	}

//	public static MoveCircle CreateFromFishInfo(FishInfo info){
//		MoveCircle result = new MoveCircle ();
//		result.StartTime = info.Created;
//		result.Velocity = info.Velocity;
//		result.StartDegree = info.MovementStartAt;
//		result.Radius = info.CircleRadius;
//		result.CenterPoint = info.CircleCenterPoint;
//	}
}
