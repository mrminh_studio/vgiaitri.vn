﻿using UnityEngine;
using System.Collections;

public class Fish : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
	
	}

	public void Die(){
		GetComponentInChildren<Animation> ().enabled = false;
		GetComponent<Move> ().enabled = false;
		transform.Rotate (new Vector3 (0, 0, 180));
		Invoke ("RemoveItseft", 2f);
	}

	public void RemoveItseft(){
		GameObject.Destroy (gameObject, 0.5f);
	}
}
