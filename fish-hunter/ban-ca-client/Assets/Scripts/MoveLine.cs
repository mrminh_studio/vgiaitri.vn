﻿using UnityEngine;
using System.Collections;
using System;

public class MoveLine : Move {

	public DateTime StartTime = DateTime.Now;
	public Vector3 StartPosition, EndPosition;
	public float StartDistance, Velocity;

	float currentDistance;

	void Start () {		
	}


	void Update () {
		currentDistance = (float)(DateTime.Now - StartTime).TotalSeconds * Velocity + StartDistance;
		float dis = currentDistance % (EndPosition - StartPosition).magnitude;
		transform.position = StartPosition + (EndPosition - StartPosition).normalized * dis;
	}

	void FixedUpdate(){
		transform.rotation = Quaternion.LookRotation((EndPosition - StartPosition), Vector3.up);	
	}

//	public static MoveLine CreateFromFishInfo(FishInfo info){
//		MoveLine result = new MoveLine ();
//		result.StartTime = info.Created;
//		result.Velocity = info.Velocity;
//		result.StartDistance = info.MovementStartAt;
//		result.StartPosition = info.LineStartPosition;
//		result.EndPosition = info.LineEndPosition;
//	}
}

