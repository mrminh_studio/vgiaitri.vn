﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameUI : MonoBehaviour {
	public static GameUI Instance;

	public Gun MainGun;
	public GameObject SubGuns;

	public GameObject PanelLogin, PanelExchange;
	public GameObject CanvasAccount;

	void Start(){
		Instance = this;
		ShowLogin ();
	}

	public void Shoot(Vector3 position, string username, int level){
		MainGun.Shoot (position);
	}

	public void ButtonLoginListener(){
		var u = PanelLogin.transform.FindChild ("Username").FindChild("Text").GetComponentInChildren<Text> ().text;
		var p = PanelLogin.transform.FindChild ("Password").FindChild("Text").GetComponentInChildren<Text> ().text;

		if (u == "")
			u = "Fucking Username" + Random.Range (0, 100000).ToString ();

		GameController.Instance.Login (u, p, (LoginResult result) => {
			if(result.Success){
				SetMoney(result.Money);
				SetUsername(result.Username);
				HideAll();
			}
		});
	}

	public void SetMoney(int money){
		transform.FindChild ("Money").gameObject.GetComponent<Text> ().text = money.ToString ();
	}

	public void SetUsername(string username){
		transform.FindChild ("Username").gameObject.GetComponent<Text> ().text = username;
	}

	public void ShowLogin(){
		
		CanvasAccount.SetActive (true);
		PanelLogin.SetActive (true);
		PanelExchange.SetActive (false);

		PanelLogin.transform.FindChild ("Username").FindChild ("Text").GetComponentInChildren<Text> ().text = PlayerPrefs.GetString ("Username", "");
	}

	public void ShowExchange(){
		CanvasAccount.SetActive (true);
		PanelLogin.SetActive (false);
		PanelExchange.SetActive (true);
	}

	public void HideAll(){
		CanvasAccount.SetActive (false);
		PanelLogin.SetActive (false);
		PanelExchange.SetActive (false);
	}
}
