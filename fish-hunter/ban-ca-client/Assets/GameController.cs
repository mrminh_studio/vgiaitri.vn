﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SocketIO;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour {
	public static GameController Instance;

	public GameObject[] Bullets;
	public GameUI UI;

	public string Username;
	public string Token;
	public int Money;

	public bool LoggedIn = false;

	SocketIOComponent socket;

	// Use this for initialization
	void Start () {
		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();
		socket.Connect ();
		socket.On("Open", (SocketIOEvent e)=>{
			Debug.Log(e);
		});

		Username = PlayerPrefs.GetString ("Username", "Username");
		Instance = this;
	}


	void OnMouseDown(){
		if (!LoggedIn)
			return;
		Shoot (Input.mousePosition, "", 0);
		UI.Shoot (Input.mousePosition, "", 0);

		var pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		Money -=  1;
		GameUI.Instance.SetMoney (Money);

		var fishes = FishManager.Instance.GetFishesAtPosition (pos);
		List<JSONObject> fishNames = new List<JSONObject>();
		foreach(Fish f in fishes){
			fishNames.Add (new JSONObject (f.gameObject.name));
		}
		JSONObject data = new JSONObject();
		data.AddField ("token", Token);
		data.AddField ("level", 0);
		data.AddField ("fishes", new JSONObject (fishNames.ToArray()));

		JSONObject jsonPos = new JSONObject ();
		jsonPos.AddField ("x", pos.x);
		jsonPos.AddField ("y", pos.y);
		jsonPos.AddField ("z", pos.z);
		data.AddField ("position", jsonPos);
		socket.Emit ("shoot fish", data, (JSONObject obj) => {

		});
	}

	public void Shoot(Vector3 position, string username, int level){
		var pos = Camera.main.ScreenToWorldPoint (position);
		pos.y *= 0.95f;
		pos.z *= 0.95f;
		GameObject go = GameObject.Instantiate (Bullets[level]);
		go.transform.SetParent (transform);
		go.transform.position = pos;
		go.GetComponent<Net> ().Username = username;
	}

	public void OtherShoot(Vector3 position, string username, int level){
		var pos = position;
		pos.y *= 0.95f;
		pos.z *= 0.95f;
		GameObject go = GameObject.Instantiate (Bullets[level]);
		go.transform.SetParent (transform);
		go.transform.position = pos;
		go.GetComponent<Net> ().Username = username;
	}

	public void StarGame(){
		for (int i = 0; i < 20; i++) {
			FishInfo info = new FishInfo ();
			info.ID = i.ToString ();
			info.TypeId = Random.Range (0, 3);
			info.MovementType = "Circle";
			info.Velocity = Random.Range(5f, 10f);
			info.CircleRadius = Random.Range (3, 10);
			info.CircleCenterPoint = Random.insideUnitSphere * 3f;
			info.CircleCenterPoint.y = 0;
			info.MovementStartAt = Random.Range (0, 360);
			info.Created = DateTime.Now;
			GetComponent<FishManager> ().CreateFish (info);
		}
	}

	public void StarGame(JSONObject json){
		foreach (JSONObject obj in json.list) {
			FishInfo info = new FishInfo ();
			info.ID = obj.GetField("ID").n.ToString();
			info.TypeId = (int)obj.GetField("TypeId").n;
			info.MovementType = "Circle";
			info.Velocity = obj.GetField("Velocity").n;
			info.CircleRadius = obj.GetField("CircleRadius").n;
			info.CircleCenterPoint = new Vector3 (obj.GetField ("CircleCenterPoint").GetField ("x").n, obj.GetField ("CircleCenterPoint").GetField ("y").n, obj.GetField ("CircleCenterPoint").GetField ("z").n);
			info.MovementStartAt = obj.GetField("MovementStartAt").n;
			info.Created = DateTime.Parse (obj.GetField ("Created").str);//, "yyyy-MM-ddThh:mm:fffZ", 	);// DateTime.Now;// DateTime.FromFileTimeUtc((long)obj.GetField ("Created").n);
			GetComponent<FishManager> ().CreateFish (info);
		}
	}


	public void Login(string username, string password, LoginCallback done){
		Dictionary<string, string> data = new Dictionary<string, string> ();
		data ["username"] = username;
		data ["password"] = password;

		socket.On ("logged in", (SocketIOEvent e) => {
			PlayerPrefs.SetString ("Username", username);

			Username = e.data.GetField("username").str;
			Token = e.data.GetField("token").str;
			Money = int.Parse(e.data.GetField("money").ToString());
			LoggedIn = true;

			done (new LoginResult {
				Success = true,
				Username = e.data.GetField("username").str,
				Money = int.Parse(e.data.GetField("money").ToString())
			});


		});

		socket.On ("sync fish", (SocketIOEvent e) => {
			Debug.Log (e);
			StarGame (e.data.GetField("fishes"));
			//StarGame ();
		});

		socket.On ("user shoot", (SocketIOEvent e) => {
			//Debug.Log (e.data);

			string usn = e.data.GetField("username").str;
			int money = (int)e.data.GetField("money").n;
			Vector3 position = new Vector3(e.data.GetField("position").GetField("x").n,
				e.data.GetField("position").GetField("y").n,
				e.data.GetField("position").GetField("z").n);

			foreach(JSONObject obj in e.data.GetField("fishes").list){
				Debug.Log(obj);
				FishManager.Instance.FishDie(obj.n.ToString());
			}

			if(usn != Username){
				Debug.Log(position);
				OtherShoot(position, usn, 0);
			}else{
				Money += money;
				GameUI.Instance.SetMoney(Money);
			}
		});

		socket.Emit("login", new JSONObject(data));
	}
}
	
public delegate void LoginCallback(LoginResult result);

public class LoginResult{
	public bool Success;
	public string Username;
	public string Token;
	public int Money;
}