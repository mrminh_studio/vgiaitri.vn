﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using Random = UnityEngine.Random;

public class FishInfo {
	public string ID;
	public int TypeId;
	public string MovementType;
	public float Velocity;
	public DateTime Created;
	public float MovementStartAt;

	public float CircleRadius;
	public Vector3 CircleCenterPoint;

	public Vector3 LineStartPosition, LineEndPosition;

}

public class FishManager : MonoBehaviour {
	public static FishManager Instance;
	public GameObject[] FishTypes;

	private Dictionary<string, Fish> fishes;

	void Start(){
		Instance = this;
		fishes = new Dictionary<string, Fish> ();
	}

	public void CreateFish(FishInfo info){
		GameObject go=null;
		if (info.MovementType == "Line") {
			go = CreateFishMoveLine (info);
		}
		if (info.MovementType == "Circle") {
			go = CreateFishMoveCircle (info);
		}

		fishes.Add (info.ID, go.GetComponent<Fish> ());
		//InvokeRepeating ("KillRandom", 1f, 1000);
	}

	public GameObject CreateFishMoveLine(FishInfo info){
		GameObject go = GameObject.Instantiate (FishTypes [info.TypeId]);
		go.name = info.ID;
		go.transform.SetParent (transform);

		var movement = go.AddComponent<MoveLine> ();

		movement.StartTime = info.Created;
		movement.Velocity = info.Velocity;
		movement.StartDistance = info.MovementStartAt;
		movement.StartPosition = info.LineStartPosition;
		movement.EndPosition = info.LineEndPosition;
		return go;
	}

	public GameObject CreateFishMoveCircle(FishInfo info){
		GameObject go = GameObject.Instantiate (FishTypes [info.TypeId]);
		go.name = info.ID;
		go.transform.SetParent (transform);

		var movement = go.AddComponent<MoveCircle> ();

		movement.StartTime = info.Created;
		movement.Velocity = info.Velocity;
		movement.StartDegree = info.MovementStartAt;
		movement.Radius = info.CircleRadius;
		movement.CenterPoint = info.CircleCenterPoint;
		return go;
	}

	public void KillRandom(){
		List<string> keys = new List<string> (fishes.Keys);
		fishes [keys [Random.Range (0, keys.Count)]].Die ();
	}

	public void FishDie(string ID){
		GameObject fish = transform.FindChild (ID).gameObject;
		fish.GetComponent<Fish> ().Die ();
		fishes.Remove (ID);
	}

	public Fish[] GetFishesAtPosition(Vector3 position){
		List<Fish> result = new List<Fish> ();
		var fishes = GetComponentsInChildren<Fish> ();
		foreach (Fish f in fishes) {
			var p = f.gameObject.transform.position;
			p.y = position.y;
			if ((p - position).magnitude < 2) {
				result.Add (f);
			}
		}

		return result.ToArray ();
	}
}	
