﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Net : MonoBehaviour {
	public string Username;

	// Use this for initialization
	void Start () {
		GetComponentInChildren<TextMesh> ().text = Username;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Destroy(){
		GameObject.Destroy (this.gameObject);
	}
}
