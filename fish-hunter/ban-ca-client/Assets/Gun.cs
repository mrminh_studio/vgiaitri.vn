﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Gun : MonoBehaviour {

	private int _level;
	public int Level{
		get{ return _level; }
		set{ _level = value; 
			if (Label != null)
				Label.text = _level.ToString ();
		}
	}

	public Text Label;
	public int DefaultLevel = 1;

	public bool Main;

	void Start(){
		Level = DefaultLevel;
	}

	public void Shoot(Vector3 pos){
		GetComponent<Animator> ().Play ("Shoot", 0, 0f);

		var direction = Camera.main.ScreenToWorldPoint(pos) - GetComponent<Image> ().rectTransform.position;
		direction.z = 0;
		float AngleRad = Mathf.Atan2(direction.y, direction.x);
		float AngleDeg = (180 / Mathf.PI) * AngleRad;
		GetComponent<Image> ().rectTransform.localRotation = Quaternion.AngleAxis(AngleDeg - 90, Vector3.forward);
	}
}
